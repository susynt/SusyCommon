The following log information is stored for the samples listed below:
1) stdout and stderr from NtMaker (`*SusyNt.log`)
2) only warnings and erros from NtMaker (`*SusyNt_warnings.log`)
3) cutflow results from [SusyNtCutflowLooper](https://gitlab.cern.ch/alarmstr/SusyNtCutflowLooper) (`*SusyNt_SusyNtCutflowLooper.log`)

# Samples
- DATA15 : `data15_13TeV.00280950.physics_Main.deriv.DAOD_SUSY2.r9264_p3083_p3704/DAOD_SUSY2.16127035._000039.pool.root.1`
- DATA16 : `data16_13TeV.00302872.physics_Main.deriv.DAOD_SUSY2.r9264_p3083_p3704/DAOD_SUSY2.16127735._000024.pool.root.1`
- DATA17 : `data17_13TeV.00337705.physics_Main.deriv.DAOD_SUSY2.r10258_p3399_p3704/DAOD_SUSY2.16129198._000057.pool.root.1`
- DATA18 : `data18_13TeV.00350880.physics_Main.deriv.DAOD_SUSY2.f937_m1972_p3704/DAOD_SUSY2.16129831._000423.pool.root.1`
- MC16A  : `mc16_13TeV.364161.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_BFilter.deriv.DAOD_SUSY2.e5340_s3126_r9364_p3875/DAOD_SUSY2.18258256._000013.pool.root.1`
- MC16D  : `mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_SUSY2.e6348_s3126_r10201_p3875/DAOD_SUSY2.18258200._000045.pool.root.1`
- MC16E  : `mc16_13TeV.364253.Sherpa_222_NNPDF30NNLO_lllv.deriv.DAOD_SUSY2.e5916_s3126_r10724_p3875/DAOD_SUSY2.18255115._000106.pool.root.1`

# Result Summary

|                        | data15 | data16 | data17 | data18 | mc16a  | mc16d  | mc16e  |
|------------------------|--------|--------|--------|--------|--------|--------|--------|
| DAOD size/event (kB)   |  18.9  |  33.3  |  51.6  |  31.7  |  34.4  |  67.4  |  50.0  |
| SusyNt size/event (kB) |  1.34  |  1.89  |  3.08  |  1.84  |  2.21  |  4.27  |  3.32  |
| Reduction              |  92.9% |  94.3% |  94.0% |  94.2% |  93.6% |  93.7% |  93.4% |
| prod. rate (Hz)        |  102   |   86   |   65   |   84   |   75   |   51   |   51   |

Notes:
- File size obtained with `du -b`
- Production rate measured locally over 10k events. Information provided SusyNtMaker output

# Replicating tests
The tests require setting up a test directory with links to the needed files and executables.
```bash
cd run
mkdir -p SusyCommonBenchmarks/prev_results
cd SusyCommonBenchmarks
ln -s ../../source/SusyCommon/test/run_ucint_tests
ln -s ../../source/SusyNtCutflowLooper/scripts/run_SusyNtCutflowLooperTests
ln -s ${path_to_test_samples} test_daod_samples
./run_ucint_tests -s data18 -o prev_results
./run_ucint_tests -s mc16e -o prev_results
./run_SusyNtCutflowLooperTests -o prev_results -i prev_results/*data18*root -i prev_results/*mc16e*root
```
If these quick tests run without crashing, then one can run the full test
```bash
./run_ucint_tests -s all -o prev_results
./run_SusyNtCutflowLooperTests  -o prev_results -i prev_results/*data18*root -i prev_results/*data17*root -i prev_results/*data16*root -i prev_results/*data15*root -i prev_results/*mc16e*root -i prev_results/*mc16d*root -i prev_results/*mc16a*root
```
The symlink `test_daod_samples` is hard coded into the `run_ucint_tests` script.
Make sure to use the same name or to change the hard coded value.

Diff the files in `prev_results/` with those found in in [SusyCommon/test/benchmark](SusyCommon/test/benchmark).
Once all differences are understood or fixed, overwrite all the old log files
```bash
cp run/SusyCommonBenchmarks/prev_results/*log source/SusyCommon/test/benchmark/
```
