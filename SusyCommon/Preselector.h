#pragma once

// C++
#include <string>
using std::string;

// xAOD
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/Photon.h"
#include "xAODMuon/Muon.h"
#include "xAODJet/Jet.h"
#include "xAODTau/TauJet.h"

// SusyNtuple
#include "SusyNtuple/Electron.h"
#include "SusyNtuple/Photon.h"
#include "SusyNtuple/Muon.h"
#include "SusyNtuple/Jet.h"
#include "SusyNtuple/Tau.h"

namespace Susy {

class Preselector {
  public:
    Preselector();
    virtual ~Preselector(){};

    // Checking if object passes preselection
    Bool_t pass(const xAOD::Electron& el) const;
    Bool_t pass(const xAOD::Photon& tau) const;
    Bool_t pass(const xAOD::Muon& mu) const;
    Bool_t pass(const xAOD::Jet& jet) const;
    Bool_t pass(const xAOD::TauJet& tau) const;

    // Returning a string with info relevant to preselections
    string infoStr(const xAOD::Electron& el) const; 
    string infoStr(const xAOD::Photon& tau) const; 
    string infoStr(const xAOD::Muon& mu) const; 
    string infoStr(const xAOD::Jet& jet) const; 
    string infoStr(const xAOD::TauJet& tau) const; 

    // Require objects be in validity range expected of tools
    Bool_t validForSF(const Susy::Electron& el) const;
    Bool_t validForChargeFlipSF(const Susy::Electron& el) const;
    
    Bool_t validForSF(const Susy::Muon& mu) const;

    Bool_t validForBtagSF(const Susy::Jet& jet) const;
    Bool_t validForJVTSF(const Susy::Jet& jet) const;
    
    Bool_t validForSF(const Susy::Tau& tau) const;

}; // class Preselector

} // namespace Susy

