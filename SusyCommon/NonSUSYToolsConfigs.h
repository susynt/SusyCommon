#pragma once

#include <string>
using std::string;
#include <map>
using std::map;

namespace SC_CONFIG {
    // Event weight files
    // These should agree with what SUSYTools uses
    extern string prw_dev_dir;
    extern map<string, string> actual_mu_files;
    extern map<string, string> grl_files;
    extern map<string, string> lumi_calc_files;

    // Charge flip tool
    extern string ChargeIDToolTrainingFile;
    extern float ChargeIDToolBDTcut;
    extern string ChargeFlipTaggerEffToolCorrectionFile(string IDWP, string IsoWP); 
    extern string ChargeFlipTaggerChIDWP;
    extern string ChargeFlipTaggerIsoWP;
    extern string EG_corrModel;

    // Jets and flavor tagging
    extern string JetCollection;
    extern string FlvTagCutDefinitionsFileName;
    extern string FlvTagScaleFactorFileName;
    extern string MCShowerID;

    // Muons
    extern float MuonSelectionToolMaxEta;
    extern bool MuSF_includeReconstructionSF;
    extern bool MuSF_includeIsolationSF;

    // Electrons
    extern bool EleSF_includeReconstructionSF;
    extern bool EleSF_includeIdentificationSF;
    extern bool EleSF_includeTriggerSF;
    extern string EleSF_trigExpression;
    extern bool EleSF_includeIsolationSF;
    extern bool EleSF_includeChargeFlipSF;
    
    // Taus
    extern bool TauSF_includeIdentificationSF;
    extern bool TauSF_includeTriggerSF;
}
