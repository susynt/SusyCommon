SusyCommon
============

A CMake package for SUSY nTuple (SusyNt) production

A better name than SusyCommon would have been UCINtMaker. The goal is to eventually change to this naming scheme after a planned refactoring of the package.

# Contents

* [Introduction](#introduction)
* [Requirements](#requirements)
* [Package Overview](#package-overview)
* [Making SusyNts](#makeing-susynts)
    * [Locally](#locally)
    * [Grid](#grid)
* [Running Benchmark Tests](#running-benchmark-tests)
* [How the Sausage is Made](#how-the-sausage-is-made)

# Introduction
For a general introduction to SUSY nTuples and a guide on how to setup the SusyNt packages, see [UCINtSetup](https://gitlab.cern.ch/alarmstr/UCINtSetup)

SusyNts are made by processing DxAODs with the help of [SUSYTools](https://gitlab.cern.ch/atlas/athena/tree/21.2/PhysicsAnalysis/SUSYPhys/SUSYTools).
The package centers around a DxAOD looper [SusyNtMaker](Root/SusyNtMaker.cxx) that inherits from ROOT [TSelector](https://root.cern.ch/developing-tselector) class.
Running the looper is done with the [NtMaker](util/NtMaker.cxx) executable (e.g. `"NtMaker -f DxAOD_file.root"`).
Most productions of SusyNts are made from SUSY2 DxAOD derivation.
However, it is possible to run on other derivations, often with minimal changes to the looper.

# Requirements
See [UCINtSetup](https://gitlab.cern.ch/alarmstr/UCINtSetup)

# Package Overview

## Classes
For an overview of the classes, see the doxygen documentation at [this
link](http://gerbaudo.github.io/SusyCommon/doxygen-html/)

SusyNtMaker
  - Inherits from XaodAnalysis
  - Applies cleaning cuts and event filtering
  - Writes a SusyNt tree

XaodAnalysis
  - Inherits from TSelector
  - Defines variables for processing DAODs
  - Defines the functions
  - Sets up all SUSYTools objects and ASG Tools

## Executables

NtMaker
  - Runs the SusyNtMaker

# Making SusyNts
The first step is to configure the SUSY Tools config file (`Base_SUSYTools_SusyNt.conf`) in [data/](data/) to whatever is needed.
Afterwards, run `./build_STconfigs` to generate the ID WP specific configuration files. 
All the script does is change the signal ID WPs for each file (possibly the baseline ID WP as well to keep it as loose as the signal). 
The reason for this is discussed below (see [How the Sausage is Made](#how-the-sausage-is-made))

## Locally
It is recommended that the executables be run within a separate `run/` directory under the top-level project.
This makes version control of the main package more straightforward and prevents the accidental inclusion of user log and data files.

To produce a SusyNt, one needs access to a DxAOD file.
This can be downloaded locally using [rucio](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/SoftwareTutorialGettingDatasets). 
It is also possible to use rucio to get an xrootd link to the file on the GRID.
`NtMaker` can accept the following as input:

* 1. path to a local root file
* 2. xrootd link to root file on the GRID
* 3. text file with list of paths to local root files
* 4. text file with lists of xrootd links to root files on the GRID
* 5. directory with root files

In options (3)-(5), `NtMaker` will build a TChain from all the root files before processing the files with `SusyNtMaker`

An example of producing SusyNts locally might look as follows:
```bash
    cd run
    mkdir QuickTest
    cd QuickTest
    rucio download ${data_sample_name} --nrandom 1
    NtMaker -f ${data_sample_name}/ -n 100
```

**Important executable arguments**

`NtMaker -h` will show you the current list of arguments.
Below a few common use cases are discussed and how to properly use the arguments 

**Case 1)** File name provided to '-f' does not contain the normal dataset identifier information

`NtMaker` parses the input file name to determine information about the sample.
It assumes the input will contain similar text to what is in a standard ATLAS DID:

- `mc16_13TeV.364253.Sherpa_222_NNPDF30NNLO_lllv.deriv.DAOD_SUSY2.e5916_s3126_r10724_p3875`
- `data17_13TeV.00337705.physics_Main.deriv.DAOD_SUSY2.r10258_p3399_p3704`
    
For example, if the sample name contains 'data' (case insensitive), the job will treat the input as data.
This is necessary when running on data so the job will not try to access MC specific branches.
When the input file name is not informative, use the '-i' option to override the sample name that will get parsed.
```base
NtMaker -f DAOD.root -i data17_13TeV.00337705.physics_Main.deriv.DAOD_SUSY2.r10258_p3399_p3704
```
The sample name is also stored as metadata in the output SusyNt so it is useful to provide a descriptive name.

<!--- TODO: Add other cases as they become needed --->

After you run NtMaker, it will spit out a root file which will contain the SusyNt TTree and additional metadata.

## GRID
For grid production of SusyNt, please see the scripts under
[susynt-submit](https://gitlab.cern.ch/susynt/susynt-submit)

# Running Benchmark Tests

Benchmark results from running over DxAOD samples for data15, data16, data17, data18, mc16a, mc16d, and mc16e are stored in [test/benchmarks](test/benchmarks).
The [README](test/benchmarks/README.md) provides details on how to replicate the benchmark tests. 
For developers, it is recommend the full set of tests be replicated and logged when submitting a new production from any tagged branch.

# How the Sausage is Made

<!--- TODO: Layout the goals of SusyNts so it is clear why things are coded the way they are --->

## Setup
**`NtMaker` call** - Build TChain from input samples, initialize/configure `SusyNtMaker`, run TTree::Process

**`SusyNtMaker::SlaveBegin`** - Open up the output root TFile and initializes the TTree 

**`XaodAnalysis::Init`** - Get needed metadata from input sample and initialize/configure the tools.

Configuring SUSYTools object (`SUSYObjDef_xAOD`):

SUSYTools objects are configured via input configuration files that get written to [data/](data) (e.g. `SUSYTools_SusyNt_eleTightLLH.conf`). 
The specifications in the files are **not** related in any straightforward way to the event and object selections applied when making SusyNts.
Instead, the configuration file are used to build separate instances of `SUSYObjDef_xAOD` for specific lepton ID working points.
These get used in tandem with independently setup ASG tools to fill the SusyNt. 
This approach allows the user to decide on object and event selections later on when processing SusyNts
[`Base_SUSYTools_SusyNt.conf`](data/Base_SUSYTools_SusyNt.conf) defines the base setup for SUSYTools and is used to generate WP specific SUSYTools instances as described [above](#making-susynts).
When running the generated `SUSYTools_SusyNt_eleTightLLH.conf` config file is the default SUSYTools object used for most purposes (e.g. getting DxAOD objects, trigger decisions, pileup reweighting, etc...)
The other instances are used only for getting scale factors for specific ID working points. 

The SusyNt variables that will **not** adjust properly to object and event selections used when processing SusyNts are as follows:
- MET - SUSYTools provides objects to the METMaker tool when they pass its own baseline requirements
- Lepton SFs - SFs are stored for each ID WP but not any other WPs (e.g. isolation). Therefore, all other SFs default to the WP specified in the config files. This slighty impacts isolation, trigger, charge, and charge flip SFs
<!--- TODO: Add to this as it becomes clear --->

## Process - Grabbing xAOD objects
**XaodAnalysis::fill_objects** - 
builds vectors of physics objects for different selections (pre, baseline, and signal). 
Only pre-objects are considered for storage in the SusyNt. Baseline and signal objects, as defined by SUSYTools, are only used for the cutflow. 
All pre-objects must pass basic preselections (`Preselector::pass`) as specified in [Preselector.cxx](Root/Preselector.cxx). 
The preselector also selects which objects are used to get scale factors (`Preselector::validForSF`) so as to avoid errors from being outside the validity range of the scale factor tools.

## Process - Setting SusyNtuple objects
**SusyNtMaker::fill_X_variables** and **SusyNtMaker::store_X** -
Loop over all pre-objects, extract relevant information to build the SusyNt object class, then store those objects for output.
The overlap removal and signal decorators from SUSYTools are reset to true so that the physics objects will be processed fully by SUSYTools functions.

## Process - MC weights


**Lepton SFs**) 

Lepton SFs are stored for each lepton and for each ID WP. 
For components of the SFs like isolation, trigger, and charge that depend on more than just the ID WP, the definition in the default SUSYTools is assumed.
Therefore, when analysing SusyNts, if one is using object selections other than what was specified in the default SUSYTools config file upon production of the SusyNt, the SFs will not be fully accurate.
The difference is often negligable and so it is usually acceptable to wait until object definitions are finalized to remake the ntuples with the correct SFs.


<!--- TODO: ## Tear Down --->

