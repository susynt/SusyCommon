#include "SusyCommon/NonSUSYToolsConfigs.h"
#include <iostream>
using std::cout;

// These should agree with what SUSYTools uses
namespace SC_CONFIG {
    ////////////////////////////////////////////////// 
    // Pileup reweighting
    // configFromFile(m_autoconfigPRWPath, "PRW.autoconfigPRWPath"
    string prw_dev_dir = "dev/PileupReweighting/share";
    // configFromFile(m_prwActualMu2017File, "PRW.ActualMu2017File"
    // configFromFile(m_prwActualMu2018File, "PRW.ActualMu2018File"
    map<string, string> actual_mu_files = {
        {"2017", "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"},
        {"2018", "GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"}
    };
    //
    map<string, string> grl_files = {
        {"2015", "GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml"},
        {"2016", "GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml"},
        {"2017", "GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"},
        {"2018", "GoodRunsLists/data18_13TeV/20181111/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"}
    };
    map<string, string> lumi_calc_files = {
        {"2015", "GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root"},
        {"2016", "GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root"},
        // dantrim September 25 2018 : actualMu files go under the "PRW configs" not the "LumiCalcFiles" but we still need to add the averageMu files
        // see this talk for clarification: https://indico.cern.ch/event/712774/contributions/2928042/attachments/1614637/2565496/prw_mc16d.pdf
        {"2017", "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"},
        {"2018", "GoodRunsLists/data18_13TeV/20181111/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-001.root"}
    };

    ////////////////////////////////////////////////// 
    // Jets and flavor tagging
    string JetCollection = "AntiKt4EMTopoJets";
    string FlvTagCutDefinitionsFileName = "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-10-19_v1.root";
    string FlvTagScaleFactorFileName = FlvTagCutDefinitionsFileName;
    string MCShowerID = "410470"; // Powheg+Pythia8 (Default)
    //string MCShowerID = "410558"; // Powheg+Herwig7
    //string MCShowerID = "426131"; // Sherpa 2.1
    //string MCShowerID = "410250"; // Sherpa 2.2

    ////////////////////////////////////////////////// 
    // Muons
    float MuonSelectionToolMaxEta = 2.7;
    bool MuSF_includeReconstructionSF = true;
    bool MuSF_includeIsolationSF = true;

    ////////////////////////////////////////////////// 
    // Electrons
    bool EleSF_includeReconstructionSF = true;
    bool EleSF_includeIdentificationSF = true;
    bool EleSF_includeTriggerSF = false; // Currently stored separately
    string EleSF_trigExpression = "singleLepton"; // Only option for SUSYTools::GetSignalElecSF()
    bool EleSF_includeIsolationSF = true;
    // charge flip SF includes charge and charge flip identification scale factors
    // only the former is included if Ele.CFT = None in ST config file
    // charge flip identification is stored separately in SusyNt so no need to set Ele.CFT
    bool EleSF_includeChargeFlipSF = false; // Stored separately 
    string ChargeFlipTaggerEffToolCorrectionFile(string IDWP, string IsoWP) { 
        if (IDWP != "MediumLLH" && IDWP != "TightLLH") {
              cout << "WARNING :: Your Electron ID WP ("+IDWP+") is not supported for ECID SFs, falling back to MediumLLH for SF purposes\n";
              IDWP = "MediumLLH";
        }
        if (IsoWP != "FCTight" && IsoWP != "Gradient") {
              cout << "Your Electron Iso WP ("+IsoWP+") is not supported for ECID SFs, falling back to Gradient for SF purposes\n";
              IsoWP = "Gradient";
        }
        return "ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/additional/"
               "efficiencySF.ChargeID."+IDWP+"_d0z0_v13_"+IsoWP+"_ECIDSloose.root";
    }

    //default cut value for https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ElectronChargeFlipTaggerTool
    string ChargeIDToolTrainingFile = "ElectronPhotonSelectorTools/ChargeID/ECIDS_20180731rel21Summer2018.root";
    float ChargeIDToolBDTcut = -0.337671; 
    string ChargeFlipTaggerChIDWP = "Loose"; // Only option available currently
    //configFromFile(m_eleIso_WP, "Ele.Iso"
    string ChargeFlipTaggerIsoWP = "Gradient"; // Should agree with ST config
    //configFromFile(m_EG_corrModel, "Ele.EffNPcorrModel"
    string EG_corrModel = "TOTAL"; // Should agree with ST config
    
    ////////////////////////////////////////////////// 
    // Taus
    bool TauSF_includeIdentificationSF = true;
    bool TauSF_includeTriggerSF = false;
}

