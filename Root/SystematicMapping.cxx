#include "SusyCommon/SystematicMapping.h"

#include <cassert>

namespace Susy {

namespace NtSys {

//-----------------------------------------
SusyNtSys CPsys2sys(const std::string &s)
{
    SusyNtSys r = SYS_UNKNOWN;
    
    
    if     ( s== "EG_RESOLUTION_ALL__1down" )                r = EG_RESOLUTION_ALL_DN;
    else if( s== "EG_RESOLUTION_ALL__1up" )                  r = EG_RESOLUTION_ALL_UP;
    else if( s== "EG_SCALE_AF2__1down" )                     r = EG_SCALE_AF2_DN;
    else if( s== "EG_SCALE_AF2__1up" )                       r = EG_SCALE_AF2_UP;
    else if( s== "EG_SCALE_ALL__1down" )                     r = EG_SCALE_ALL_DN;
    else if( s== "EG_SCALE_ALL__1up" )                       r = EG_SCALE_ALL_UP;
    
    else if( s== "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down" )         r = EL_EFF_ID_TOTAL_Uncorr_DN;
    else if( s== "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up" )           r = EL_EFF_ID_TOTAL_Uncorr_UP; 
    else if( s== "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down" )        r = EL_EFF_Iso_TOTAL_Uncorr_DN; 
    else if( s== "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up" )          r = EL_EFF_Iso_TOTAL_Uncorr_UP; 
    else if( s== "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down" )       r = EL_EFF_Reco_TOTAL_Uncorr_DN; 
    else if( s== "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up" )         r = EL_EFF_Reco_TOTAL_Uncorr_UP; 
    else if( s== "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down")     r = EL_EFF_Trigger_TOTAL_DN; 
    else if( s== "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up")       r = EL_EFF_Trigger_TOTAL_UP; 
    
    else if( s== "FT_EFF_B_systematics__1down" )             r = FT_EFF_B_systematics_DN;
    else if( s== "FT_EFF_B_systematics__1up" )               r = FT_EFF_B_systematics_UP;
    else if( s== "FT_EFF_C_systematics__1down" )             r = FT_EFF_C_systematics_DN;
    else if( s== "FT_EFF_C_systematics__1up" )               r = FT_EFF_C_systematics_UP;
    else if( s== "FT_EFF_Light_systematics__1down" )         r = FT_EFF_Light_systematics_DN;
    else if( s== "FT_EFF_Light_systematics__1up" )           r = FT_EFF_Light_systematics_UP;
    else if( s== "FT_EFF_extrapolation__1down" )             r = FT_EFF_extrapolation_DN;
    else if( s== "FT_EFF_extrapolation__1up" )               r = FT_EFF_extrapolation_UP;
    else if( s== "FT_EFF_extrapolation_from_charm__1down" )  r = FT_EFF_extrapolation_charm_DN;
    else if( s== "FT_EFF_extrapolation_from_charm__1up" )    r = FT_EFF_extrapolation_charm_UP;
    
    else if( s== "JET_JER_DataVsMC__1up" )                   r = JET_JER_DataVsMC_UP;
    else if( s== "JET_JER_DataVsMC__1down" )                 r = JET_JER_DataVsMC_DN;
    else if( s== "JET_JER_EffectiveNP_1__1up" )              r = JET_JER_EffectiveNP_1_UP;
    else if( s== "JET_JER_EffectiveNP_1__1down" )            r = JET_JER_EffectiveNP_1_DN;
    else if( s== "JET_JER_EffectiveNP_2__1up" )              r = JET_JER_EffectiveNP_2_UP;
    else if( s== "JET_JER_EffectiveNP_2__1down" )            r = JET_JER_EffectiveNP_2_DN;
    else if( s== "JET_JER_EffectiveNP_3__1up" )              r = JET_JER_EffectiveNP_3_UP;
    else if( s== "JET_JER_EffectiveNP_3__1down" )            r = JET_JER_EffectiveNP_3_DN;
    else if( s== "JET_JER_EffectiveNP_4__1up" )              r = JET_JER_EffectiveNP_4_UP;
    else if( s== "JET_JER_EffectiveNP_4__1down" )            r = JET_JER_EffectiveNP_4_DN;
    else if( s== "JET_JER_EffectiveNP_5__1up" )              r = JET_JER_EffectiveNP_5_UP;
    else if( s== "JET_JER_EffectiveNP_5__1down" )            r = JET_JER_EffectiveNP_5_DN;
    else if( s== "JET_JER_EffectiveNP_6__1up" )              r = JET_JER_EffectiveNP_6_UP;
    else if( s== "JET_JER_EffectiveNP_6__1down" )            r = JET_JER_EffectiveNP_6_DN;
    else if( s== "JET_JER_EffectiveNP_7restTerm__1up" )          r = JET_JER_EffectiveNP_7rest_UP;
    else if( s== "JET_JER_EffectiveNP_7restTerm__1down" )        r = JET_JER_EffectiveNP_7rest_DN;
    else if( s== "JET_JER_EffectiveNP_7__1up" )              r = JET_JER_EffectiveNP_7_UP;
    else if( s== "JET_JER_EffectiveNP_7__1down" )            r = JET_JER_EffectiveNP_7_DN;
    else if( s== "JET_JER_EffectiveNP_8__1up" )              r = JET_JER_EffectiveNP_8_UP;
    else if( s== "JET_JER_EffectiveNP_8__1down" )            r = JET_JER_EffectiveNP_8_DN;
    else if( s== "JET_JER_EffectiveNP_9__1up" )              r = JET_JER_EffectiveNP_9_UP;
    else if( s== "JET_JER_EffectiveNP_9__1down" )            r = JET_JER_EffectiveNP_9_DN;
    else if( s== "JET_JER_EffectiveNP_10__1up" )              r = JET_JER_EffectiveNP_10_UP;
    else if( s== "JET_JER_EffectiveNP_10__1down" )            r = JET_JER_EffectiveNP_10_DN;
    else if( s== "JET_JER_EffectiveNP_11__1up" )              r = JET_JER_EffectiveNP_11_UP;
    else if( s== "JET_JER_EffectiveNP_11__1down" )            r = JET_JER_EffectiveNP_11_DN;
    else if( s== "JET_JER_EffectiveNP_12restTerm__1up" )      r = JET_JER_EffectiveNP_12rest_UP;
    else if( s== "JET_JER_EffectiveNP_12restTerm__1down" )    r = JET_JER_EffectiveNP_12rest_DN;

    else if( s== "JET_EffectiveNP_1__1up")                  r = JET_EffectiveNP_1_UP;
    else if( s== "JET_EffectiveNP_1__1down")                r = JET_EffectiveNP_1_DN;
    else if( s== "JET_EffectiveNP_2__1up")                  r = JET_EffectiveNP_2_UP;
    else if( s== "JET_EffectiveNP_2__1down")                r = JET_EffectiveNP_2_DN;
    else if( s== "JET_EffectiveNP_3__1up")                  r = JET_EffectiveNP_3_UP;
    else if( s== "JET_EffectiveNP_3__1down")                r = JET_EffectiveNP_3_DN;
    else if( s== "JET_EffectiveNP_4__1up")                  r = JET_EffectiveNP_4_UP;
    else if( s== "JET_EffectiveNP_4__1down")                r = JET_EffectiveNP_4_DN;
    else if( s== "JET_EffectiveNP_5__1up")                  r = JET_EffectiveNP_5_UP;
    else if( s== "JET_EffectiveNP_5__1down")                r = JET_EffectiveNP_5_DN;
    else if( s== "JET_EffectiveNP_6__1up")                  r = JET_EffectiveNP_6_UP;
    else if( s== "JET_EffectiveNP_6__1down")                r = JET_EffectiveNP_6_DN;
    else if( s== "JET_EffectiveNP_7__1up")                  r = JET_EffectiveNP_7_UP;
    else if( s== "JET_EffectiveNP_7__1down")                r = JET_EffectiveNP_7_DN;
    else if( s== "JET_EffectiveNP_8restTerm__1up")          r = JET_EffectiveNP_8rest_UP;
    else if( s== "JET_EffectiveNP_8restTerm__1down")        r = JET_EffectiveNP_8rest_DN;
    else if( s== "JET_EtaIntercalibration_Modelling__1up")  r = JET_EtaIntercalibration_Modelling_UP;
    else if( s== "JET_EtaIntercalibration_Modelling__1down")r = JET_EtaIntercalibration_Modelling_DN;
    else if( s== "JET_EtaIntercalibration_NonClosure_highE__1up")       r = JET_EtaIntercalibration_NonClosure_highE_UP;
    else if( s== "JET_EtaIntercalibration_NonClosure_highE__1down")     r = JET_EtaIntercalibration_NonClosure_highE_DN;
    else if( s== "JET_EtaIntercalibration_NonClosure_negEta__1up")      r = JET_EtaIntercalibration_NonClosure_negEta_UP;
    else if( s== "JET_EtaIntercalibration_NonClosure_negEta__1down")    r = JET_EtaIntercalibration_NonClosure_negEta_DN;
    else if( s== "JET_EtaIntercalibration_NonClosure_posEta__1up")      r = JET_EtaIntercalibration_NonClosure_posEta_UP;
    else if( s== "JET_EtaIntercalibration_NonClosure_posEta__1down")    r = JET_EtaIntercalibration_NonClosure_posEta_DN;
    else if( s== "JET_EtaIntercalibration_TotalStat__1up")              r = JET_EtaIntercalibration_TotalStat_UP;
    else if( s== "JET_EtaIntercalibration_TotalStat__1down")            r = JET_EtaIntercalibration_TotalStat_DN;

    else if( s== "JET_Flavor_Composition__1up")         r = JET_Flavor_Composition_UP;
    else if( s== "JET_Flavor_Composition__1down")       r = JET_Flavor_Composition_DN;
    else if( s== "JET_Flavor_Response__1up")            r = JET_Flavor_Response_UP;
    else if( s== "JET_Flavor_Response__1down")          r = JET_Flavor_Response_DN;
    else if( s== "JET_BJES_Response__1up")              r = JET_BJES_Response_UP;
    else if( s== "JET_BJES_Response__1down")            r = JET_BJES_Response_DN;
    else if( s== "JET_Pileup_OffsetMu__1up")            r = JET_Pileup_OffsetMu_UP;
    else if( s== "JET_Pileup_OffsetMu__1down")            r = JET_Pileup_OffsetMu_DN;
    else if( s== "JET_Pileup_OffsetNPV__1up")           r = JET_Pileup_OffsetNPV_UP;
    else if( s== "JET_Pileup_OffsetNPV__1down")           r = JET_Pileup_OffsetNPV_DN;
    else if( s== "JET_Pileup_PtTerm__1up")              r = JET_Pileup_PtTerm_UP;
    else if( s== "JET_Pileup_PtTerm__1down")              r = JET_Pileup_PtTerm_DN;
    else if( s== "JET_Pileup_RhoTopology__1up")         r = JET_Pileup_RhoTopology_UP;
    else if( s== "JET_Pileup_RhoTopology__1down")         r = JET_Pileup_RhoTopology_DN;
    else if( s== "JET_PunchThrough_MC16__1up")          r = JET_PunchThrough_MC16_UP;
    else if( s== "JET_PunchThrough_MC16__1down")          r = JET_PunchThrough_MC16_DN;
    else if( s== "JET_SingleParticle_HighPt__1up")      r = JET_SingleParticle_HighPt_UP;
    else if( s== "JET_SingleParticle_HighPt__1down")    r = JET_SingleParticle_HighPt_DN;
    

    else if( s== "JET_GroupedNP_1__1up" )                    r = JET_GroupedNP_1_UP;
    else if( s== "JET_GroupedNP_1__1down" )                  r = JET_GroupedNP_1_DN;
    else if( s== "JET_GroupedNP_2__1up" )                    r = JET_GroupedNP_2_UP;
    else if( s== "JET_GroupedNP_2__1down" )                  r = JET_GroupedNP_2_DN;
    else if( s== "JET_GroupedNP_3__1up" )                    r = JET_GroupedNP_3_UP;
    else if( s== "JET_GroupedNP_3__1down" )                  r = JET_GroupedNP_3_DN;
    else if( s== "JET_JvtEfficiency__1up" )                  r = JET_JVTEff_UP;
    else if( s== "JET_JvtEfficiency__1down" )                r = JET_JVTEff_DN;
    else if( s== "JET_EtaIntercalibration_NonClosure__1up" ) r = JET_EtaIntercalibration_UP;
    else if( s== "JET_EtaIntercalibration_NonClosure__1down") r = JET_EtaIntercalibration_DN;
    
    else if( s== "MET_SoftTrk_ResoPara" )                    r = MET_SoftTrk_ResoPara;
    else if( s== "MET_SoftTrk_ResoPerp" )                    r = MET_SoftTrk_ResoPerp;
    else if( s== "MET_SoftTrk_ScaleDown" )                   r = MET_SoftTrk_ScaleDown;
    else if( s== "MET_SoftTrk_ScaleUp" )                     r = MET_SoftTrk_ScaleUp;

    else if( s== "MUON_EFF_BADMUON_STAT__1down" )            r = MUON_EFF_BADMUON_STAT_DN;
    else if( s== "MUON_EFF_BADMUON_STAT__1up" )              r = MUON_EFF_BADMUON_STAT_UP;
    else if( s== "MUON_EFF_BADMUON_SYS__1down" )             r = MUON_EFF_BADMUON_SYS_DN;
    else if( s== "MUON_EFF_BADMUON_SYS__1up" )               r = MUON_EFF_BADMUON_SYS_UP;
    else if( s== "MUON_EFF_ISO_STAT__1down" )               r = MUON_EFF_ISO_STAT_DN;
    else if( s== "MUON_EFF_ISO_STAT__1up" )                 r = MUON_EFF_ISO_STAT_UP;
    else if( s== "MUON_EFF_ISO_SYS__1down" )                r = MUON_EFF_ISO_SYS_DN;
    else if( s== "MUON_EFF_ISO_SYS__1up" )                  r = MUON_EFF_ISO_SYS_UP;
    else if( s== "MUON_EFF_RECO_STAT__1down" )              r = MUON_EFF_RECO_STAT_DN;
    else if( s== "MUON_EFF_RECO_STAT__1up" )                r = MUON_EFF_RECO_STAT_UP;
    else if( s== "MUON_EFF_RECO_STAT_LOWPT__1down" )        r = MUON_EFF_RECO_STAT_LOWPT_DN;
    else if( s== "MUON_EFF_RECO_STAT_LOWPT__1up" )          r = MUON_EFF_RECO_STAT_LOWPT_UP;
    else if( s== "MUON_EFF_RECO_SYS__1down" )               r = MUON_EFF_RECO_SYS_DN;
    else if( s== "MUON_EFF_RECO_SYS__1up" )                 r = MUON_EFF_RECO_SYS_UP;
    else if( s== "MUON_EFF_RECO_SYS_LOWPT__1down" )         r = MUON_EFF_RECO_STAT_LOWPT_DN;
    else if( s== "MUON_EFF_RECO_SYS_LOWPT__1up" )           r = MUON_EFF_RECO_STAT_LOWPT_UP;
    else if( s== "MUON_EFF_TTVA_STAT__1down" )              r = MUON_EFF_TTVA_STAT_DN;
    else if( s== "MUON_EFF_TTVA_STAT__1up" )                r = MUON_EFF_TTVA_STAT_UP;
    else if( s== "MUON_EFF_TTVA_SYS__1down" )               r = MUON_EFF_TTVA_SYS_DN;
    else if( s== "MUON_EFF_TTVA_SYS__1up" )                 r = MUON_EFF_TTVA_SYS_UP;
    else if( s== "MUON_EFF_TrigStatUncertainty__1down" )    r = MUON_EFF_TrigStat_DN;
    else if( s== "MUON_EFF_TrigStatUncertainty__1up" )      r = MUON_EFF_TrigStat_UP;
    else if( s== "MUON_EFF_TrigSystUncertainty__1down" )    r = MUON_EFF_TrigSys_DN;
    else if( s== "MUON_EFF_TrigSystUncertainty__1up" )      r = MUON_EFF_TrigSys_UP;
    else if( s== "MUON_ID__1down" )                         r = MUON_ID_DN;
    else if( s== "MUON_ID__1up" )                           r = MUON_ID_UP;
    else if( s== "MUON_MS__1down" )                         r = MUON_MS_DN;
    else if( s== "MUON_MS__1up" )                           r = MUON_MS_UP;
    else if( s== "MUON_SAGITTA_RESBIAS__1down" )            r = MUON_SAGITTA_RESBIAS_DN;
    else if( s== "MUON_SAGITTA_RESBIAS__1up" )              r = MUON_SAGITTA_RESBIAS_UP;
    else if( s== "MUON_SAGITTA_RHO__1down" )                r = MUON_SAGITTA_RHO_DN;
    else if( s== "MUON_SAGITTA_RHO__1up" )                  r = MUON_SAGITTA_RHO_UP;
    else if( s== "MUON_SCALE__1down" )                      r = MUON_SCALE_DN;
    else if( s== "MUON_SCALE__1up" )                        r = MUON_SCALE_UP;

    else if( s== "TAUS_TRUEHADTAU_SME_TES_DETECTOR__1down" ) r = TAU_SME_TES_DET_DN;
    else if( s== "TAUS_TRUEHADTAU_SME_TES_DETECTOR__1up" )   r = TAU_SME_TES_DET_UP;
    else if( s== "TAUS_TRUEHADTAU_SME_TES_INSITU__1down" )   r = TAU_SME_TES_INSITU_DN;
    else if( s== "TAUS_TRUEHADTAU_SME_TES_INSITU__1up" )     r = TAU_SME_TES_INSITU_UP;
    else if( s== "TAUS_TRUEHADTAU_SME_TES_MODEL__1down" )    r = TAU_SME_TES_MODEL_DN;
    else if( s== "TAUS_TRUEHADTAU_SME_TES_MODEL__1up" )      r = TAU_SME_TES_MODEL_UP;
    
    // PileupReweighting (mu) variations
    else if( s== "PRW_DATASF__1down" )                       r = PILEUP_DN;
    else if( s== "PRW_DATASF__1up" )                         r = PILEUP_UP;
    
    return r;
}



} //Ntsys
} // susy
