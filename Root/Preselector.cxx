// Preselections should be as tight as those found in SUSYTools
#include "SusyCommon/Preselector.h"

// C++
#include <sstream> // stringstream

const static SG::AuxElement::Decorator<char> acc_baseline("baseline");

namespace Susy {

Preselector::Preselector() {}

////////////////////////////////////////////////////////////////////////////////
// Electrons
////////////////////////////////////////////////////////////////////////////////
Bool_t Preselector::pass(const xAOD::Electron& el) const {
    if (el.pt() < 4000 /*MeV*/) return false;
    if (!el.isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON)) return false;

    return true;
}

string Preselector::infoStr(const xAOD::Electron& el) const {
    std::stringstream ss;
    
    ss << "pt = " << el.pt() << "MeV; "; 
    
    ss << "GoodOQ(BADCLUSELECTRON) = " 
       << el.isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON) << "; "; 
    
    ss << "\n";
    return ss.str();
}

Bool_t Preselector::validForSF(const Susy::Electron& el) const {
    if (el.Pt() < 4.0 /*GeV*/) return false;
    if (!(el.looseLLH || el.looseLLHBLayer || el.mediumLLH || el.tightLLH)) return false;
    return true;
}

Bool_t Preselector::validForChargeFlipSF(const Susy::Electron& el) const {
    // Range found by running SUSYTools' m_elecChargeEffCorrTool in VERBOSE mode.
    // It printed '|eta| limits 0, 2.5' and 'pt limits 20, 999'
    // SUSYTools will still dump errors because it first sets the SF in FillElectron
    if (el.Pt() < 20.0 || 1000.0 < el.Pt() /*GeV*/) return false;
    if (fabs(el.Eta()) > 2.5 ) return false;
    return true;
}

////////////////////////////////////////////////////////////////////////////////
// Muons
////////////////////////////////////////////////////////////////////////////////
Bool_t Preselector::pass(const xAOD::Muon& mu) const {
    
    if (mu.pt() < 3000) return false;

    // don't treat SAF muons without CB track further
    Bool_t SAF = mu.muonType() == xAOD::Muon::SiliconAssociatedForwardMuon;
    Bool_t combined = mu.trackParticle(xAOD::Muon::CombinedTrackParticle);
    if (SAF && !combined) return false;

    return true;
}

string Preselector::infoStr(const xAOD::Muon& mu) const {
    std::stringstream ss;

    ss << "pt = " << mu.pt() << "MeV; "; 

    Bool_t SAF = mu.muonType() == xAOD::Muon::SiliconAssociatedForwardMuon;
    Bool_t combined = (bool)mu.trackParticle(xAOD::Muon::CombinedTrackParticle);
    ss << "SiliconAssociatedForwardMuon = " << SAF << " and "
       << "CombinedTrackParticle = " << combined << "; "; 
    
    ss << "\n";
    return ss.str();
}

Bool_t Preselector::validForSF(const Susy::Muon& mu) const {
    // See https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCPAnalysisWinterMC16#Efficiency_corrections
    if (mu.Pt() < 4.5 /*GeV*/) return false;
    if (!(mu.loose || mu.medium || mu.tight)) return false;
    if (mu.isStandalone && (2.5 < fabs(mu.Eta()) || fabs(mu.Eta()) < 2.7)) return false; //TTVA SFs not defined here
    return true;
}

////////////////////////////////////////////////////////////////////////////////
// Jets
////////////////////////////////////////////////////////////////////////////////
Bool_t Preselector::pass(const xAOD::Jet& jet) const {
    if(jet.pt() < 20000.0 /*MeV*/) return false; 
    
    return true;
}

string Preselector::infoStr(const xAOD::Jet& jet) const {
    std::stringstream ss;
    
    ss << "pt = " << jet.pt() << "MeV;"; 
    
    ss << "\n";
    return ss.str();
}

Bool_t Preselector::validForBtagSF(const Susy::Jet& jet) const {
    if (jet.Pt() < 20 || 2000 < jet.Pt() /*GeV*/) return false;
    if (fabs(jet.Eta()) > 2.5) return false;
    return true;
}

Bool_t Preselector::validForJVTSF(const Susy::Jet& /*jet*/) const {
    // JVT SF tool applies an inefficiency SF if the standard JVT cuts are not satisfied
    // No need to apply cuts here
    return true;
}

////////////////////////////////////////////////////////////////////////////////
// Taus
////////////////////////////////////////////////////////////////////////////////
Bool_t Preselector::pass(const xAOD::TauJet& tau) const {
    if (std::abs(tau.charge()) != 1) return false;
    
    int nTrk = tau.nTracks();
    if (nTrk!=1 && nTrk!=3 && nTrk!=5) return false;

    if (!acc_baseline(tau)) return false;
    return true;
}

string Preselector::infoStr(const xAOD::TauJet& tau) const {
    std::stringstream ss;
    
    ss << "ST baseline = " << acc_baseline(tau) << "; "; 
    
    ss << "\n";
    return ss.str();
}

Bool_t Preselector::validForSF(const Susy::Tau& /*tau*/) const {
    return true;
}

////////////////////////////////////////////////////////////////////////////////
// Photons
////////////////////////////////////////////////////////////////////////////////
Bool_t Preselector::pass(const xAOD::Photon& /*pho*/) const {
    // No selection
    return true;
}

string Preselector::infoStr(const xAOD::Photon& /*pho*/) const {
    std::stringstream ss;
    
    ss << "No Selections; "; 
    
    ss << "\n";
    return ss.str();
}
} // namespace Susy

